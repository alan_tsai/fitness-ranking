import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

const Vue = createApp(App);
Vue.mount("#app");
